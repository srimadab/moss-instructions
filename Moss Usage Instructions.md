# MOSS Usage Instructions

#### Registration and Setup
1. If you do not have a moss account send an email to `moss@moss.stanford.edu`. The body of the message should have only two lines and must appear exactly as follows.

    registeruser

    mail your_email
    
2. In a few hours, you should be receiving an email with the moss script that has your unique userid in it. 
3. Copy the entire script, paste in a file and name it as `moss` without any extensions.
4. Set execution permission on the file by running `chmod ug+x moss`

#### Usage

1. From the Project description page, click on `Download Submissions` option in the top-right corner.
2. Insert the downloaded zip file in a new empty folder.
3. Unzip the file.

    `unzip <project_name.zip>`

4. Remove project zip file from directory.

    `rm <project_name.zip>`

5. Unzip all the student project files with correct name/folder.

    `for file in *.zip; do unzip "$file" -d "${file%.zip}"; done`

6. Remove the individual project zip files that were unzipped.

    `rm *.zip`

7. Cleaning up by renaming the files to remove spaces from the names (works by recursively removing spaces between file/folder names replacing them with underscores).

    `find . -name "* *" | awk '{ print length, $0 }' | sort -nr -s | cut -d" " -f2- | while read f; do base=$(basename "$f"); newbase="${base// /_}"; mv "$(dirname "$f")/$(basename "$f")" "$(dirname "$f")/$newbase"; done`

8. Remove DS Store | __MACOSX files (which MOSS doesn’t like).

    `find . -name ".DS_Store" -exec rm -rf '{}' +`

    `find . -name "__MACOSX" -exec rm -rf '{}' +`

9. Set up environment variable $FILES which is a link to all the code files inside the project. If working with java files, change the extension ".py" to ".java" in the command below.

    `FILES=$(find path_to_project_folder -type f -name “*.py")`

    Example: `FILES=$(find /Users/srinidhimadabhushi/Desktop/project1 -type f -name “*.py")`

10. Submit the files to MOSS by running MOSS script. If working with java files, change "python" to "java" in the command below.

    For mac users: `./moss -l python -d $FILES`

    For windows users: `moss -l python -d $FILES`

11. Wait for server response, this can take upto 15 minutes. Once received, redirect to the link provided in the response.


#### MOSS Original website
https://theory.stanford.edu/~aiken/moss/

**Instructions written by Lombe Chileshe and Nidhi Madabhushi**
